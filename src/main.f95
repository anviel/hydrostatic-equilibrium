! ------------------------------------------------------------------------------
! File       : main.f95
! Purpose    : compute the hydrostatic pressure inside planets
! Author     : A. Viel, Club d'Astronomie Jupiter du Roannais
! Created on : 2020-APR-18
! Modified on: 2020-APR-20
! Remark     :
! The bodies are defined in a file bodies.dat, its format is the following:
!  format (I2,1X,A20), number of layers, followed by name of body
!  format (F4.0), the outer radius of each layer [km]
!  format (F4.1), the density at bottom of layer [g/cm**3]
!  format (F4.1), the density at top of layer [g/cm**3]
! Reference  : WOOLFSON M., Planetary Science, IOP Publishing, 2002, p. 331
! ------------------------------------------------------------------------------
program main
use model
implicit none
integer, parameter :: nlmax = 12   ! maximum number of layers
integer, parameter :: nsample = 24 ! number of points in each layer
double precision, dimension(nlmax) :: r
double precision, dimension(nlmax) :: rho_b
double precision, dimension(nlmax) :: rho_t
double precision, dimension(nlmax) :: m, &  ! mass enclosed by layer
                                      p     ! pressure at bottom of layer
double precision, dimension(nsample*nlmax) :: rprof, pprof
integer :: i, nlayer, ios
character(20) :: bname
character(160) :: filename

! get filename as first argument
call get_command_argument(1, filename)
if (len_trim(filename) .eq. 0) then
   print *, "Usage: hydrostatic <filename.dat>"
   stop
endif

open(unit=19, file=filename, form='formatted', action='read')
! loop over all the bodies
do
   ! read number of layers, and name of body
   read(19, 4600, iostat=ios) nlayer, bname 
   if (ios .lt. 0) then
      exit  ! enf of file
   endif
   ! read outer radius of current layer [km]
   ! read density at bottom of current layer [g/cm**3]
   ! read density at top of current layer [g/cm**3]
   do i = 1, nlayer
      read(19, 4650) r(i), rho_b(i), rho_t(i)
   enddo
   ! unit conversion to S.I.
   r = 1000*r         ! km to m
   rho_b = 1000*rho_b ! g/cm**3 to kg/m**3
   rho_t = 1000*rho_t ! g/cm**3 to kg/m**3
   ! compute mass, pressure & display
   print '(A20)', bname
   call compute_mass_pressure_shell(nlayer, r, rho_b, rho_t, m, p)
   call display_mass_pressure(nlayer, r, m, p)
   ! compute the pressure profile function
   call compute_pressure_profile(nlayer, r, rho_b, rho_t, &
                                 nsample, rprof, pprof)
   ! write it into a file
   call write_pressure_profile(trim(bname)//'.dat', &
                               nlayer*nsample, rprof, pprof)
enddo
close(unit=19)

4600 format(I2, 1X, A20)
4650 format(F4.0, 1X, F4.1, 1X, F4.1)
end program main

subroutine display_mass_pressure(nlayer, r, m, p)
implicit none
integer, intent(in) :: nlayer
double precision, dimension(nlayer), intent(in) :: r
double precision, dimension(nlayer), intent(in) :: m
double precision, dimension(nlayer), intent(in) :: p
integer :: i
double precision :: r_disp, m_disp, p_disp
! display header with units
!         123456789012345678901234567890
print *, '  r [km]    m [kg]   p [kbar]'
! display values at center of planet (zero mass, infinite pressure)
print 4700, 0D0, 0D0, huge(0D0)
! display the values for the other layers
do i = 1, nlayer
   if (i .lt. nlayer) then
      r_disp = r(i)/1D3
      m_disp = m(i)
      p_disp = p(i+1)/1D8
   else
      r_disp = r(i)/1D3
      m_disp = m(i)
      p_disp = 0D0   ! zero relative pressure at planet surface
   endif 
   ! display radius [km], enclosed mass [kg], pressure [kbar]
   print 4700, r_disp, m_disp, p_disp
enddo
4700 format(F10.0, E10.3, F10.1)
end subroutine display_mass_pressure

subroutine write_pressure_profile(filename, npts, rprof, pprof)
implicit none
character(*), intent(in) :: filename
integer, intent(in) :: npts
double precision, dimension(npts), intent(in) :: rprof, pprof
integer :: i
open(unit=20, file=trim(filename), form='formatted', action='write')
do i = 2, npts
   write (20, 4700) rprof(i)/1D3, &  ! radius [km]
                    (rprof(npts)-rprof(i))/1D3, & ! depth [km]
                    pprof(i)/1D8     ! pressure [kbar]
enddo
close(unit=20)
4700 format(F10.0,1X,F10.0,1X,F10.1)
end subroutine write_pressure_profile
