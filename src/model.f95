! ------------------------------------------------------------------------------
! File       : model.f95
! Purpose    : Model of hydrostatic equilibrium in planets.
!              This module define subroutines for computing the mass profile
!              function and the hydrostatic pressure profile function
! Author     : A. Viel, Club d'Astronomie Jupiter du Roannais
! Modified on: 2020-MAR-30
! Created on : 2020-APR-18
! ------------------------------------------------------------------------------
module model
contains
!
! Compute the mass enclosed by each shell r < r(i)
!
subroutine compute_mass_shell(nlayer, r, rho_b, rho_t, mr)
implicit none
integer, intent(in) :: nlayer
double precision, dimension(nlayer), intent(in) :: r
double precision, dimension(nlayer), intent(in) :: rho_b
double precision, dimension(nlayer), intent(in) :: rho_t

double precision, dimension(nlayer), intent(out)   :: mr  ! enclosed < r

double precision, parameter :: PI = 4D0*atan(1D0)

integer :: i
double precision :: r_b, m_b
double precision :: delta_r, delta_rho, ki

! compute the mass enclosed by r < r(i)
m_b = 0D0
r_b = 0D0
do i = 1, nlayer
   delta_r   = r(i) - r_b
   delta_rho = rho_t(i) - rho_b(i)
   ki        = rho_b(i)*r(i) - rho_t(i)*r_b
   ! integrate mass from below layer
   !   dm = 4*pi * r**2 * rho(r) * dr
   ! between r_b and r(i)
   ! rho(r) interpolates linearly (r_b, rho_b) and (r(i), rho_t)
   mr(i) = m_b + 4D0*PI/delta_r*(delta_rho/4D0*(r(i)**4-r_b**4) &
                                 + ki/3D0*(r(i)**3-r_b**3))
   m_b = mr(i)
   r_b = r(i)
enddo
end subroutine compute_mass_shell

!
! Compute pressure inside a shell
!
subroutine compute_pressure(r, r_b, r_t, rho_b, rho_t, &
                            m_b, p_t, p_r)
implicit none
double precision, intent(in) :: r, &  ! desired radius
                         r_b, r_t, &  ! bottom & top radius
                     rho_b, rho_t, &  ! densities at r_b and r_t
                              m_b, &  ! mass enclosed r<r_b
                              p_t     ! pressure at r=r_t
double precision, intent(out) :: p_r  ! pressure at radius r
double precision :: delta_r, delta_rho, ki, &
                    drdr, dr2dr2, drdr2, ki2dr2
double precision, parameter :: PI = 4D0*atan(1D0)
double precision, parameter :: G = 6.673D-11  ! m**3/kg/s**2
! pre-compute some common factors
delta_r   = r_t - r_b
delta_rho = rho_t - rho_b
ki        = rho_b*r_t - rho_t*r_b
drdr   = delta_rho/delta_r
dr2dr2 = drdr**2
drdr2  = delta_rho/(delta_r**2)
ki2dr2 = (ki/delta_r)**2
! integrate pressure from above layer
!   dp = - G rho(r) m(r) dr / r**2
! between r and r_t
p_r = p_t + G*( PI*dr2dr2*(r_t**4-r**4)/4D0 &
                + 7D0*PI/3D0*ki*drdr2*(r_t**3-r**3)/3D0 &
                + 4D0*PI/3D0*ki2dr2*(r_t**2-r**2)/2D0  &
                + (m_b*drdr - PI*dr2dr2*r_b**4 &
                   - 4D0*PI/3D0*ki*drdr2*r_b**3)*log(r_t/r) &
                + (m_b*ki/delta_r - PI*ki*drdr2*r_b**4 &
                   - 4D0*PI/3D0*ki2dr2*r_b**3)*(1D0/r-1D0/r_t) )
end subroutine compute_pressure

!
! Compute the mass enclosed and pressure for all shell interfaces r(i)
!
subroutine compute_mass_pressure_shell(nlayer, r, rho_b, rho_t, mr, pr)
implicit none
integer, intent(in) :: nlayer
double precision, dimension(nlayer), intent(in) :: r
double precision, dimension(nlayer), intent(in) :: rho_b
double precision, dimension(nlayer), intent(in) :: rho_t

double precision, dimension(nlayer), intent(out)   :: mr  ! enclosed < r
double precision, dimension(nlayer-1), intent(out) :: pr  ! at bottom

integer :: i
double precision :: p_t
double precision :: delta_r, delta_rho, ki

! compute the mass enclosed by r < r(i)
call compute_mass_shell(nlayer, r, rho_b, rho_t, mr)

! compute the pressure at bottom of layer i
p_t = 0D0
do i = nlayer, 2, -1
   call compute_pressure(r(i-1), r(i-1), r(i), &
                         rho_b(i), rho_t(i), mr(i-1), p_t, pr(i))
   p_t = pr(i)
enddo
pr(1) = 0D0   ! should be infinity
end subroutine compute_mass_pressure_shell

!
! Compute the whole pressure profile function from r=0 to r=Rplanet
!
subroutine compute_pressure_profile(nlayer, r, rho_b, rho_t, &
                                    nsample, rprof, pprof)
implicit none
integer, intent(in) :: nlayer ! the number of shells
double precision, dimension(nlayer), intent(in) :: r
double precision, dimension(nlayer), intent(in) :: rho_b
double precision, dimension(nlayer), intent(in) :: rho_t
integer, intent(in) :: nsample ! the number of points in each shell
double precision, dimension(nsample*nlayer), intent(out) :: rprof
double precision, dimension(nsample*nlayer), intent(out) :: pprof

double precision, dimension(nlayer) :: m_r  ! enclosed < r
integer :: i, j, ij
double precision :: p_t, j01, r_b, m_b

! compute the mass enclosed by r < r(i)
call compute_mass_shell(nlayer, r, rho_b, rho_t, m_r)

! sample the pressure nsamples time inside each shell
p_t = 0D0
do i = nlayer, 1, -1
   if (i .eq. 1) then
      r_b = 0D0
      m_b = 0D0
   else
      r_b = r(i-1)
      m_b = m_r(i-1)
   endif
   do j = 1, nsample
      ij = (i-1)*nsample+j
      j01 = (real(j) - 1D0)/real(nsample-1)
      rprof(ij) = r_b*(1D0-j01) + r(i)*j01
      call compute_pressure(rprof(ij), r_b, r(i), &
                            rho_b(i), rho_t(i), &
                            m_b, p_t, &
                            pprof(ij))
   enddo
   ! continuity condition:
   ! pressure at top of next shell = pressure at bottom of current shell
   p_t = pprof((i-1)*nsample+1)
enddo
end subroutine compute_pressure_profile

end module model
