# Hydrostatic equilibrium
Compute the hydrostatic pressure in the interior of spherically symmetric bodies.

## Dependencies
- [GNU Fortran](http://gcc.gnu.org/fortran/)
- [gnuplot](http://www.gnuplot.info)

## Build
In the `src` directory, simply launch `make`, this should produce a `hydrostatic` binary.

## Getting started
Take the `data/bodies.dat` input data file which define the interior of three celestial bodies: the Earth, the Moon and Mercury.

Call the `hydrostatic` binary, passing this file as argument:
```
./hydrostatic data/bodies.dat
```
This produces the following output:

```
Earth               
   r [km]    m [kg]   p [kbar]
        0. 0.000E+00**********
     1278. 0.113E+24    3228.6
     3488. 0.192E+25    1366.3
     5708. 0.487E+25     254.3
     6343. 0.600E+25       6.8
     6370. 0.603E+25       0.0
Moon                
   r [km]    m [kg]   p [kbar]
        0. 0.000E+00**********
      382. 0.187E+22      48.5
     1688. 0.676E+23       2.5
     1738. 0.733E+23       0.0
Mercury             
   r [km]    m [kg]   p [kbar]
        0. 0.000E+00**********
     1830. 0.200E+24      86.1
     2290. 0.299E+24      15.1
     2440. 0.327E+24       0.0
```

Each line corresponds to a layer of the planeteray interior. The first column is the radius of this layer, in kilometer, the second column the mass enclosed below this radius, in kilogram, and the third column the hydrostatic pressure at exactly this radius, in kilobar. Notice that pressure at the center is not displayed, as the formula is singular for zero radius.

Three data files are also produced: `Earth.dat`, `Moon.dat` and `Mercury.dat`.
These files contain the pressure profile from the center to the surface, with 24 samples per layer. It can be plotted using the provided script:
```
./plot_pressure.sh Earth.dat
```
![Earth hydrostatic pressure](images/Earth_hydrostatic_pressure.png)

## Principle of the method
All details about the assumptions, model, formula and references are explained in the `doc/hydrostatic.pdf` document.

## Body description file format
The interior of a body can be described in a Fortran formatted file, with the following format:
- First line (format I2, 1X, A20) gives the number of layers (maximum 12), followed by the name of the body
- The other lines (format F4.0, 1X, F4.1, 1X, F4.1) define the layers. There is exactly one line per layer. First float number is the outer radius of the current layer, expressed in kilometer, followed by two float numbers, one is the density at the bottom of layer, and the other the density at the top of layer, expressed in gram per cubic centimer.

The input file may define several bodies, one after another.

## Example
Consider for example the following input data file:
```
3  Moon
 382  8.0  8.0
1688  3.3  3.3
1738  3.1  3.1
```
This example defines the interior of the Moon as having three layers:
- first layer (core) has a radius of 382 km, and constant density of 8 g/cm^3 (same density at top and bottom)
- second layer (mantle) has an outer radius of 1688 km, and constant density of 3.3 g/cm^3
- third layer (crust) has an outer radius (the Moon radius) of 1738 km, and constant density of 3.1 g/cm^3

Notice that the name of the body, here Moon, is used to create the pressure profile file, which is `Moon.dat`.
