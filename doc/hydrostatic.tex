% ------------------------------------------------------------------------------
% File       : hydrostatic.tex
% Description: Model of hydrostatic conditions in planets
% Created on : 18-APR-2020
% Modified on: 
% ------------------------------------------------------------------------------
\documentclass{article}
\usepackage{a4}
%\usepackage[french]{babel}
\usepackage[T1]{fontenc}
\usepackage{units}
\usepackage{tikz}
\usepackage{amsmath}
%\usepackage{amssymb}

\setlength{\parindent}{0mm}

\begin{document}
\title{A model of hydrostatic conditions in planets}
\author{A. Viel\footnote{Club d'Astronomie Jupiter du Roannais}}
\date{2020-04-20}
\maketitle

\section{Assumptions of the model.}

\begin{itemize}
\item Planets considererd in this model are perfectly spherical
\item Planets are considered in hydrostatic equilibrium from their surface down to the center \cite{Woolfson}
\item Planets are made of shells (numbered $i = 1, ..., n$) defined by their bottom radius $r_{b,i}$ and top radius $r_{t,i}$. As a consequence, the bottom radius of the first shell is $r_{b,1} = 0$, and the top radius of the last shell $r_{t,n}$ is the planet radius $R$, as shown on figure (\ref{fig:planet})
\item The density function is spherically symmetric
\item The density function may be discontinuous across two shells
\item The density function is linear in a shell, it is defined by a linear law which interpolates between the bottom density $\rho_{b,i}$ and the top density $\rho_{t,i}$
\end{itemize}
\begin{equation}
\rho_i(r) = \frac{\Delta \rho_i\,r + \chi_i}{\Delta r_i} \hspace{1cm}\mbox{for } r_{b,i} \leq r \leq r_{t,i}
\end{equation}
with $\Delta r_i = r_{t,i} - r_{b,i}$, $\Delta \rho_i = \rho_{t,i} - \rho_{b,i}$, and $\chi_i = \rho_{b,i}\,r_{t,i} - \rho_{t,i}\,r_{b,i}$.

\begin{figure}[htbp]
\begin{center}
\begin{tikzpicture}
\draw (0,0) -- (10,0);
\draw (0,0) -- (7.66, 6.43);
\draw (10,0) arc [start angle=0, end angle=40, radius=10];
\draw (6,0) arc [start angle=0, end angle=40, radius=6];
\draw (3.5,0) arc [start angle=0, end angle=40, radius=3.5];
\draw (9.5,0) arc [start angle=0, end angle=40, radius=9.5];
\draw (4.46,1.62) node {Shell $i$};
\draw (3.5,-0.4) node {$r_{t,i-1} = r_{b,i}$};
\draw (6,-0.4) node {$r_{t,i} = r_{b,i+1}$};
\draw (0,-0.4) node {$r_{b,1} = 0$};
\draw (10,-0.4) node {$r_{t,n} = R$};
\end{tikzpicture}
\end{center}
\caption{Interior of a planet of outer radius $R$, made of $n = 4$ symmetrically spherical shells.}
\label{fig:planet}
\end{figure}

\section{Mass profile function.}
The mass profile function $m(r)$ gives the mass enclosed below a given radius $r$.
Its variation with radius is given by:
\begin{equation}
\frac{dm}{dr}(r) = 4\,\pi\,r^2\,\rho(r)
\label{eq:dmdr}
\end{equation}
with the boundary condition at the center $m(0) = 0$.

Inside a shell, the mass profile function is then obtained by integrating (\ref{eq:dmdr}):
\begin{align}
m(r) &= m(r_b) + 4\,\pi\,\int_{r_b}^{r_t} r^2\,\frac{\Delta \rho\,r + \chi}{\Delta r}\,dr \nonumber\\
&= m(r_b) + \frac{4\,\pi}{\Delta r}\,(\Delta\rho\,\frac{r^4-r_b^4}{4} + \chi\,\frac{r^3-r_b^3}{3})
\end{align}

Moreover, $m(R)$ is the total mass of the planet.

\section{Hydrostatic pressure.}
Hydrostatic equilibrium is expressed by the following differential equation:
\begin{equation}
\frac{dp}{dr}(r) = -G\,\frac{\rho(r)\,m(r)}{r^2}
\label{eq:hydroeq}
\end{equation}
with $p(r)$ the hydrostatic pressure at radius $r$ and $G = \unit[6.673\times10^{-11}]{m^3.kg^{-1}.s^{-2}}$ the gravitational constant.
The boundary condition is defined at surface:
\begin{equation}
p(r_{t,n}) = 0
\end{equation}

Inside a shell, the hydrostatic pressure is obtained by integrating downward the equation (\ref{eq:hydroeq}):
\begin{equation}
p(r_t) - p(r) = -G\,\int_r^{r_t} \frac{\rho(r)\,m(r)}{r^2}\,dr
\end{equation}
and thus,
\begin{equation}
p(r) = p(r_t) + G\,I(r)
\end{equation}
with
\begin{align}
I(r) &= \int_r^{r_t} \frac{\rho(r)\,m(r)}{r^2}\,dr \nonumber\\
&= \pi\,(\frac{\Delta\rho}{\Delta r})^2\,\frac{r_t^4 - r^4}{4} \nonumber\\
&\;\;\;+\frac{7\,\pi}{3}\,\frac{\chi\,\Delta\rho}{(\Delta r)^2}\,\frac{r_t^3 - r^3}{3} \nonumber\\
&\;\;\;+\frac{4\,\pi}{3}\,(\frac{\chi}{\Delta r})^2\,\frac{r_t^2 - r^2}{2} \nonumber\\
&\;\;\;+ [m(r_b)\,\frac{\Delta\rho}{\Delta r} - \pi\,(\frac{\Delta\rho}{\Delta r})^2\,r_b^4 - \frac{4\,\pi}{3}\,\frac{\chi\,\Delta\rho}{(\Delta r)^2}\,r_b^3]\,\log(\frac{r_t}{r}) \nonumber\\
&\;\;\;+ [m(r_b)\,\frac{\chi}{\Delta r} - \pi\,\frac{\chi\,\Delta\rho}{(\Delta r)^2}\,r_b^4 - \frac{4\,\pi}{3}\,(\frac{\chi}{\Delta r})^2\,r_b^3]\,(\frac{1}{r}-\frac{1}{r_t})
\end{align}

\section{Example: hydrostatic pressure in the Earth.}
According to the Preliminary Reference Earth Model (PREM, \cite{PREM}),
the Earth is made of five layers that are defined in the following table:
\begin{center}
\begin{tabular}{|c|r|r|r|r|r|}
\hline
layer & radius (km) & depth (km) & \multicolumn{2}{c|}{densities (g/cm$^3$)} & mass ($\unit[10^{24}]{kg}$)\\
 & & & bottom & top &\\
\hline
inner core & 1278 & 5092 & 13.1 & 12.8 & 1.13\\
outer core & 3488 & 2882 & 12.2 & 9.9 & 1.92\\
lower mantle & 5708 & 662 & 5.6 & 4.4 & 4.87\\
upper mantle & 6343 & 27 & 4.4 & 3.4 & 6.00\\
crust & 6370 & 0 & 2.9 & 2.2 & 6.03\\
\hline
\end{tabular}
\end{center}
The mass enclosed by the last layer (the crust) is $m(R) = \unit[6.03\times10^{24}]{kg}$, which must be compared with the known (or measured) mass of the Earth, which is $\unit[5.973\times10^{24}]{kg}$. This yields an error of \unit[0.95]{\%}, which gives an estimate of the accuracy of the model combined with the PREM datas.

The pressure profile is shown on figure (\ref{fig:pprofile}).
\begin{figure}[hbp]
\begin{center}
\includegraphics[width=14cm]{Earth.png}
\end{center}
\caption{Earth hydrostatic pressure from the surface to the center}
\label{fig:pprofile}
\end{figure}


\begin{thebibliography}{99}
\bibitem{PREM} Dziewonsky, A.M., Anderson D.L., \emph{Preliminary Earth Reference Model}, Phys. Earth Planet. Int., 25, 297-356, 1981.
\bibitem{Woolfson} Woolfson, M., \emph{Planetary sciences}, IoP Publishing, 2002.
\end{thebibliography}

\end{document}
